from dataclasses import dataclass, field
from enum import Enum
from typing import Callable
import config as cfg

@dataclass
class Sensor:
    class Type(Enum):
        SENSOR          = 'sensor'
        BINARY_SENSOR   = 'binary_sensor'

    name:           str
    device_name:    str
    subtopic:       str
    data_path:      tuple
    type:           Type
    device:         dict = None
    device_class:   str = None
    unit:           str = None
    converter:      Callable[[str], None] = lambda x: x

    def config_topic(self):
        return f'homeassistant/{self.type.value}/{self.device_name}/{self._hass_topic()}/config'

    def config_payload(self):
        payload = { "name": self.name }

        if self.unit:
            payload["unit_of_meas"] = self.unit

        if self.device and isinstance(self.device, dict):
            payload['dev'] = self.device

        payload['stat_t'] = self.state_topic()
        payload['uniq_id'] = f'{self.device_name}_{self._hass_topic()}'
        payload['obj_id'] = f'{self.device_name}.{self._hass_topic()}'
        return str(payload).replace("'", '"')

    def state_topic(self):
        return f'homeassistant/{self.type.value}/{self.device_name}/{self._hass_topic()}/state'

    def state_payload(self, data):
        raw = data

        for key in self.data_path:
            raw = raw[key]

        if self.converter:
            return self.converter(raw)
        else:
            return raw

    def _hass_topic(self):
        return self.subtopic.replace('/', '_')    # flatten the hierarchy for HASS autodiscovery


# COP estimation based on the datasheet values
class SensorCOP(Sensor):
    def state_payload(self, data):
        tA = float(data['stat']['temps']['zew']) / 10
        tW = float(data['params']['cricuits'][0]['twatersetheat']) / 10
        return round(0.118 * tA - 0.1 * tW + 7.5889, 2)
