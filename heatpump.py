import requests
import json
import logging


class TemperatureSettings:
    # Maksymalne temperatury wody zasilajacej wzgledem temperatury powietrza na zewnatrz
    MAX_WATER_TEMP = {
        'Tm20': 50,
        'Tm10': 55, # 50*C wg HMI
        'T0':   60, # 38*C wg HMI
        'Tp15': 60, # 34*C wg HMI
    }

    # Minimalne temperatury wody zasilajacej wzgledem temperatury powietrza na zewnatrz
    MIN_WATER_TEMP = {
        'Tm20': 20, # 38*C wg HMI
        'Tm10': 20, # 34*C wg HMI
        'T0':   20, # 26*C wg HMI
        'Tp15': 20,
    }

    # Zakres regulowanych temperatur powietrza (grzania)
    MAX_SETPOINT = 28
    MIN_SETPOINT = 16

    # Nazwy punktow na krzywej grzania (uzywane przez API)
    CURVE_POINTS = ('Tm20', 'Tm10', 'T0', 'Tp15')


    @staticmethod
    def check_curve(curve):
        for p in TemperatureSettings.CURVE_POINTS:
            if not p in curve.keys():
                raise RuntimeError(f'Missing "{p}" curve point')

        for key, val in curve.items():
            if val > TemperatureSettings.MAX_WATER_TEMP[key]:
                raise RuntimeError(f'{key} setpoint is too high ({str(val)} *C, max {TemperatureSettings.MAX_WATER_TEMP[key]} *C)')

            if val < TemperatureSettings.MIN_WATER_TEMP[key]:
                raise RuntimeError(f'{key} setpoint is too low ({str(val)} *C, min {TemperatureSettings.MIN_WATER_TEMP[key]} *C)')

        return True


    @staticmethod
    def check_setpoint(temperature):
        if temperature > TemperatureSettings.MAX_SETPOINT:
            raise RuntimeError(f'Temperature setpoint is too high ({temperature} *C, max {TemperatureSettings.MAX_SETPOINT} *C)')

        if temperature < TemperatureSettings.MIN_SETPOINT:
            raise RuntimeError(f'Temperature setpoint is too low ({temperature} *C, min {TemperatureSettings.MIN_SETPOINT} *C)')

        return True


    def __init__(self, curve, setpoint):
        TemperatureSettings.check_curve(curve)
        TemperatureSettings.check_setpoint(setpoint)
        self._curve = curve
        self._setpoint = setpoint


    @property
    def curve(self):
        return self._curve


    @curve.setter
    def curve(self, curve):
        TemperatureSettings.check_curve(curve)
        self._curve = curve


    @property
    def setpoint(self):
        return self._setpoint


    @setpoint.setter
    def setpoint(self, setpoint):
        TemperatureSettings.check_setpoint(setpoint)
        self._setpoint = setpoint


    def __str__(self):
        return f'setpoint={self.setpoint} *C | curve={str(self.curve)}'


class HeatpumpLazar:
    URL = 'https://hkslazar.net'

    def __init__(self, login, password, verify_ssl=True, test=False):
        self._logger = logging.getLogger('HeatpumpLazar')
        self._login = login
        self._password = password
        self._test = test

        if verify_ssl:
            self._certificate = 'hkslazar.pem'
        else:
            self._certificate = False
            # Suppress expired SSL certificate warning
            from urllib3.exceptions import InsecureRequestWarning
            requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

        self._restart_session()


    def _restart_session(self):
        self._logger.debug('Restarting session')
        self._session = requests.Session()
        response = self._session.post(f'{HeatpumpLazar.URL}/sollogin',
            data={'login': self._login, 'password': self._password},
            verify=self._certificate)

        if response.status_code != 200 or 'incorrect' in response.text:
            self._session = None
            raise RuntimeError('Authentication error')


    def _send_request(self, link):
        if self._test:
            self._logger.info(f'Request in test mode: {link}')
            return {'result': 'success'}    # pretend everything is ok

        response = self._session.get(link)

        if (response.status_code != 200):
            raise RuntimeError(f'Request error: {response.status_code}/{response.text}')

        return json.loads(response.text)


    def _set_param(self, data, circuit_idx=None):
        if circuit_idx and circuit_idx != 0 and circuit_idx != 1:
            raise RuntimeError('Circuit index must be either 0 or 1')

        link = f'{HeatpumpLazar.URL}/oemSerwis?what=setparam'

        if circuit_idx:
            link += f'&idx={circuit_idx}'

        link += '&param='

        for name in data.keys():
            link += f'{name},'

        link += '&value='

        for value in data.values():
            link += f'{value},'

        response = self._send_request(link)

        if 'logout' in response:    # session has expired
            self._restart_session()
            response = self._send_request(link)

        if 'result' not in response or response['result'] != 'success':
            raise RuntimeError(f'Setting parameter failed ({response[reason]})')


    def get_raw_status(self):
        return self._send_request(f'{HeatpumpLazar.URL}/oemSerwis?what=bcst')


    def set_curve(self, circuit_idx, curve):
        TemperatureSettings.check_curve(curve)
        data = {}

        for name, value in curve.items():
            data[f'tsetcrv{name}'] = str(value * 10)

        try:
            self._set_param(data, circuit_idx)
            self._logger.debug(f'set_curve({circuit_idx}, {str(curve)}) executed')
        except Exception as e:
            self._logger.error(f'set_curve({circuit_idx}, {str(curve)}): {str(e)}')


    def _set_temp(self, mode, circuit_idx, temperature):
        TemperatureSettings.check_setpoint(temperature)

        try:
            self._set_param({mode: str(temperature * 10)}, circuit_idx)
            self._logger.debug(f'_set_temp({mode}, {circuit_idx}, {temperature}) executed')
        except Exception as e:
            self._logger.error(f'_set_temp({mode}, {circuit_idx}, {temperature}): {str(e)}')


    def set_temp_eco(self, circuit_idx, temperature):
        self._set_temp('tsetecoheat', circuit_idx, temperature)


    def set_temp_comfort(self, circuit_idx, temperature):
        self._set_temp('tsetcomfheat', circuit_idx, temperature)


    def set_enable(self, state):
        if state:
            state = 'on'
        else:
            state = 'off'

        try:
            self._set_param({'onoff': state})
            self._logger.info(f'set_enable({state}) executed')
        except Exception as e:
            self._logger.error(f'set_enable({state}): {str(e)}')


    def apply_settings(self, circuit_idx, settings):
        self.set_curve(circuit_idx, settings.curve)
        self.set_temp_eco(circuit_idx, settings.setpoint)
        self.set_temp_comfort(circuit_idx, settings.setpoint)
