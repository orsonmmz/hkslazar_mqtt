#!/usr/bin/env python3
# TODO proper logging
# TODO requirements
import requests
import time
import json
import sys
import paho.mqtt.client as mqtt
import config as cfg
from mqtt_mockup import MqttMockup
from sensor import *

if len(sys.argv) > 1:
    sim_mode = True
    sim_file = sys.argv[1]
    print(f'Simulation mode: {sim_file}')
else:
    sim_mode = False


hks_url = 'https://hkslazar.net'
heat_pump_modes = ('Heating', 'Cooling', 'DHW')
secure = True       # verify the SSL certificate

# Custom converters
converter_on_off        = lambda x: 'ON' if x == 1 else 'OFF'
converter_decimal       = lambda x: x / 10.0
converter_str_join      = lambda x: '\n'.join(x)
converter_alarms        = lambda x: ' | '.join("{0} ({1})".format(i["desc"], i["id"]) for i in x)
converter_valve_4way    = lambda x: 'HEATING' if x == 1 else 'COOLING'

device = {
        'identifiers':  [cfg.hass_unique_id],
        'manufacturer': 'HKS Lazar',
        'model':        'HTi20',
        'name':         'HKS Lazar HTi20'
}

sensors = [
    # Temperature sensors
    Sensor(name=f'{device["model"]} Inlet',                         subtopic='temp/inlet',       data_path=('stat', 'temps', 'ret'),        device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),    # temperatura powrotu
    Sensor(name=f'{device["model"]} Outlet',                        subtopic='temp/outlet',      data_path=('stat', 'temps', 'out'),        device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),    # temperatura zasilania
    Sensor(name=f'{device["model"]} External',                      subtopic='temp/external',    data_path=('stat', 'temps', 'zew'),        device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    Sensor(name=f'{device["model"]} Compressor Evaporation',        subtopic='compevap',         data_path=('stat', 'temps', 'compevap'),    device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),   # temperatura parowania
    Sensor(name=f'{device["model"]} Compressor Suction',            subtopic='compsuc',          data_path=('stat', 'temps', 'compsuc'),     device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),   # temperatura ssania
    # Water pumps
    Sensor(name=f'{device["model"]} Water Pump 1',  subtopic='pump/1/state',     data_path=('stat', 'pumps', 'pumpcir1'),   device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.BINARY_SENSOR, converter=converter_on_off), # pompa obiegowa
    Sensor(name=f'{device["model"]} Water Pump 2',  subtopic='pump/2/state',     data_path=('stat', 'pumps', 'pumpcir2'),   device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.BINARY_SENSOR, converter=converter_on_off), # pompa obiegowa
    # Alarms
    Sensor(name=f'{device["model"]} Alarm Counter', subtopic='alarm/count',      data_path=('stat', 'alarmcnt'),            device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR),
    Sensor(name=f'{device["model"]} Alarm Message', subtopic='alarm/msg',        data_path=('stat', 'alarms'),              device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, converter=converter_alarms),
    # General status
    Sensor(name=f'{device["model"]} Active',        subtopic='active',           data_path=('params', 'onoff'),             device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.BINARY_SENSOR, converter=converter_on_off),
    Sensor(name=f'{device["model"]} Mode',          subtopic='mode',             data_path=('params', 'mode'),              device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, converter=lambda x: heat_pump_modes[int(x)]),
    Sensor(name=f'{device["model"]} Timeout',       subtopic='timeout',          data_path=('timeout',),                    device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR),
    Sensor(name=f'{device["model"]} Error',         subtopic='error',            data_path=('error',),                      device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR),
    Sensor(name=f'{device["model"]} Fan',           subtopic='fan_speed',        data_path=('stat', 'unit', 'fan'),         device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, unit="%", converter=converter_decimal),  # moc wentylatora
    Sensor(name=f'{device["model"]} Power',         subtopic='powerneed',        data_path=('stat', 'unit', 'powerneed'),   device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, unit="%", converter=converter_decimal),
    Sensor(name=f'{device["model"]} Compressor',    subtopic='comprpow',         data_path=('stat', 'unit', 'comprpow'),    device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, unit="%", converter=converter_decimal), # moc sprezarki
    Sensor(name=f'{device["model"]} EEV Opening (Cooling)',         subtopic='eevcool',                  data_path=('stat', 'unit', 'eevcool'),      device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, unit="steps"),   # otwarcie zaworu EEV chlodzenie, kroki
    Sensor(name=f'{device["model"]} EEV Opening (Heating)',         subtopic='eevheat',                  data_path=('stat', 'unit', 'eevheat'),      device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, unit="steps"),   # otwarcie zaworu EEV grzanie, kroki
    Sensor(name=f'{device["model"]} Room 1 Thermostat Switch',      subtopic='roomswitch1',              data_path=('stat', 'other', 'roomswitch1'), device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.BINARY_SENSOR, converter=converter_on_off),   # termostat stykowy
    Sensor(name=f'{device["model"]} Room 2 Thermostat Switch',      subtopic='roomswitch2',              data_path=('stat', 'other', 'roomswitch2'), device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.BINARY_SENSOR, converter=converter_on_off),
    Sensor(name=f'{device["model"]} 4-way Valve',                   subtopic='valve4way',                data_path=('stat', 'other', 'valve4way'),   device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, converter=converter_valve_4way),   # zawor 4-drogowy, 1=grzanie 0=?
    # Heaters
    Sensor(name=f'{device["model"]} Drip Tray Heater',              subtopic='heaters/leak',             data_path=('stat', 'heaters', 'leak'),      device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.BINARY_SENSOR, converter=converter_on_off),   # grzalka tacy ociekowej
    Sensor(name=f'{device["model"]} Carter Heater',                 subtopic='heaters/carter',           data_path=('stat', 'heaters', 'carter'),    device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.BINARY_SENSOR, converter=converter_on_off),   # grzalka karteru
    Sensor(name=f'{device["model"]} Flow Heater Level 1',           subtopic='heaters/flowlvl1',         data_path=('stat', 'heaters', 'flowlvl1'),  device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.BINARY_SENSOR, converter=converter_on_off),   # grzalka 1 stopien
    Sensor(name=f'{device["model"]} Flow Heater Level 2',           subtopic='heaters/flowlvl2',         data_path=('stat', 'heaters', 'flowlvl2'),  device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.BINARY_SENSOR, converter=converter_on_off),   # grzalka 2 stopien
    # Circuit 1
    Sensor(name=f'{device["model"]} C1 Room Sensor',                subtopic='circuit/1/room/sensor',    data_path=('params', 'cricuits', 0, 'troom'),         device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    Sensor(name=f'{device["model"]} C1 Room Cooling Setpoint',      subtopic='circuit/1/room/cool',      data_path=('params', 'cricuits', 0, 'troomsetcool'),  device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    Sensor(name=f'{device["model"]} C1 Room Heating Setpoint',      subtopic='circuit/1/room/heat',      data_path=('params', 'cricuits', 0, 'troomsetheat'),  device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    Sensor(name=f'{device["model"]} C1 Heating Eco Setpoint',       subtopic='circuit/1/heat/eco',       data_path=('params', 'cricuits', 0, 'tsetecoheat'),   device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    Sensor(name=f'{device["model"]} C1 Heating Comfort Setpoint',   subtopic='circuit/1/heat/comf',      data_path=('params', 'cricuits', 0, 'tsetcomfheat'),  device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    Sensor(name=f'{device["model"]} C1 Heating Water Setpoint',     subtopic='circuit/1/heat/water',     data_path=('params', 'cricuits', 0, 'twatersetheat'), device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    Sensor(name=f'{device["model"]} C1 Cooling Eco Setpoint',       subtopic='circuit/1/cool/eco',       data_path=('params', 'cricuits', 0, 'tsetecocool'),   device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    Sensor(name=f'{device["model"]} C1 Cooling Comfort Setpoint',   subtopic='circuit/1/cool/comf',      data_path=('params', 'cricuits', 0, 'tsetcomfcool'),  device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    Sensor(name=f'{device["model"]} C1 Cooling Water Setpoint',     subtopic='circuit/1/cool/water',     data_path=('params', 'cricuits', 0, 'twatersetcool'), device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    Sensor(name=f'{device["model"]} C1 Temperature Curve +15°C',    subtopic='circuit/1/curve/15',       data_path=('params', 'cricuits', 0, 'tsetcrvTp15'),   device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    Sensor(name=f'{device["model"]} C1 Temperature Curve 0°C',      subtopic='circuit/1/curve/0',        data_path=('params', 'cricuits', 0, 'tsetcrvT0'),     device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    Sensor(name=f'{device["model"]} C1 Temperature Curve -10°C',    subtopic='circuit/1/curve/-10',      data_path=('params', 'cricuits', 0, 'tsetcrvTm10'),   device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    Sensor(name=f'{device["model"]} C1 Temperature Curve -20°C',    subtopic='circuit/1/curve/-20',      data_path=('params', 'cricuits', 0, 'tsetcrvTm20'),   device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    # Circuit 2
    Sensor(name=f'{device["model"]} C2 Room Sensor',                subtopic='circuit/2/room/sensor',    data_path=('params', 'cricuits', 1, 'troom'),         device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    Sensor(name=f'{device["model"]} C2 Room Cooling Setpoint',      subtopic='circuit/2/room/cool',      data_path=('params', 'cricuits', 1, 'troomsetcool'),  device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    Sensor(name=f'{device["model"]} C2 Room Heating Setpoint',      subtopic='circuit/2/room/heat',      data_path=('params', 'cricuits', 1, 'troomsetheat'),  device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    Sensor(name=f'{device["model"]} C2 Heating Eco Setpoint',       subtopic='circuit/2/heat/eco',       data_path=('params', 'cricuits', 1, 'tsetecoheat'),   device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    Sensor(name=f'{device["model"]} C2 Heating Comfort Setpoint',   subtopic='circuit/2/heat/comf',      data_path=('params', 'cricuits', 1, 'tsetcomfheat'),  device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    Sensor(name=f'{device["model"]} C2 Heating Water Setpoint',     subtopic='circuit/2/heat/water',     data_path=('params', 'cricuits', 1, 'twatersetheat'), device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    Sensor(name=f'{device["model"]} C2 Cooling Eco Setpoint',       subtopic='circuit/2/cool/eco',       data_path=('params', 'cricuits', 1, 'tsetecocool'),   device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    Sensor(name=f'{device["model"]} C2 Cooling Comfort Setpoint',   subtopic='circuit/2/cool/comf',      data_path=('params', 'cricuits', 1, 'tsetcomfcool'),  device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    Sensor(name=f'{device["model"]} C2 Cooling Water Setpoint',     subtopic='circuit/2/cool/water',     data_path=('params', 'cricuits', 1, 'twatersetcool'), device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    Sensor(name=f'{device["model"]} C2 Temperature Curve +15°C',    subtopic='circuit/2/curve/15',       data_path=('params', 'cricuits', 1, 'tsetcrvTp15'),   device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    Sensor(name=f'{device["model"]} C2 Temperature Curve 0°C',      subtopic='circuit/2/curve/0',        data_path=('params', 'cricuits', 1, 'tsetcrvT0'),     device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    Sensor(name=f'{device["model"]} C2 Temperature Curve -10°C',    subtopic='circuit/2/curve/-10',      data_path=('params', 'cricuits', 1, 'tsetcrvTm10'),   device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    Sensor(name=f'{device["model"]} C2 Temperature Curve -20°C',    subtopic='circuit/2/curve/-20',      data_path=('params', 'cricuits', 1, 'tsetcrvTm20'),   device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR, device_class='temperature', unit="°C", converter=converter_decimal),
    # Unknown variables
    Sensor(name=f'{device["model"]} tfinaloutlet',  subtopic='unknown/tfinaloutlet', data_path=('params', 'tfinaloutlet'),       device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR),
    Sensor(name=f'{device["model"]} modeCalCWU',    subtopic='unknown/modeCalCWU',   data_path=('stat', 'other', 'modeCalCWU'),  device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR),
    Sensor(name=f'{device["model"]} modeCalCO',     subtopic='unknown/modeCalCO',    data_path=('stat', 'other', 'modeCalCO'),   device=device, device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR),
    # Calculated sensors
    SensorCOP(name=f'{device["model"]} COP',        subtopic='cop',                  data_path=None, device=device,device_name=cfg.hass_unique_id, type=Sensor.Type.SENSOR),
]


# MQTT init
if sim_mode:
    mqtt_client = MqttMockup(client_id="hkslazar", transport="tcp", protocol=mqtt.MQTTv311)
else:
    mqtt_client = mqtt.Client(client_id="hkslazar", transport="tcp", protocol=mqtt.MQTTv311)

mqtt_client.connect(cfg.mqtt_broker, port=cfg.mqtt_port, keepalive=60)
mqtt_client.loop_start()


# HASS autodiscovery
for s in sensors:
    mqtt_client.publish(topic=s.config_topic(), payload=s.config_payload(), qos=cfg.mqtt_qos, retain=True)

print('HomeAssistant autodiscovery configured')

certificate = None

if secure:
    certificate = 'hkslazar.pem'
else:
    certificate = False
    # Suppress expired SSL certificate warning
    from urllib3.exceptions import InsecureRequestWarning
    requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

if sim_mode:
    # Process fake data
    with open(sim_file) as f:
        data = json.load(f)

        for s in sensors:
            mqtt_client.publish(topic=s.state_topic(), payload=s.state_payload(data),
                    qos=cfg.mqtt_qos, retain=True)
else:
    run = True
    while run:
        try:
            # Authenticate to hkslazar.net
            with requests.Session() as session:
                response = session.post(hks_url + '/sollogin',
                    data={'login': cfg.hks_user, 'password': cfg.hks_password},
                    verify=certificate)

                if (response.status_code != 200):
                    raise RuntimeError('Authentication error')

                # Main loop
                reauthenticate = False
                print("Authenticated, starting the main loop")

                while not reauthenticate:
                    try:
                        # get data from the website
                        data_raw = session.get(hks_url + '/oemSerwis?what=bcst')
                        data = json.loads(data_raw.text)
                        all_wrong = True
                        #data = json.loads('\n'.join(open('example.raw', 'r').readlines())) # TODO remove

                        for s in sensors:
                            try:
                                mqtt_client.publish(topic=s.state_topic(), payload=s.state_payload(data),
                                        qos=cfg.mqtt_qos, retain=True)
                                all_wrong = False
                            except Exception as e:
                                print('Could not publish topic {0}: {1}'.format(s.state_topic(), e))

                        #print('published')
                    except Exception as e:
                        print(str(e))

                    time.sleep(cfg.update_period)

                    if all_wrong:       # try to reauth if no data is correct
                        time.sleep(5 * 60)
                        reauthenticate = True

        except KeyboardInterrupt:
            run = False
            print('Terminating the script')
        except Exception as e:
            print(str(e))
            time.sleep(30)

mqtt_client.loop_stop()
mqtt_client.disconnect()
