import schedule
import datetime
import logging
from enum import Enum

class Program(Enum):
    SUSTAIN = "sustain",
    LOW     = "low",
    MEDIUM  = "medium",
    HIGH    = "high"


class HeatpumpController:
    def __init__(self, heatpump, circuit, longitude, latitude):
        self._heatpump = heatpump
        self._longitude = longitude
        self._latitude = latitude
        self._circuit = circuit
        self._program_settings = {}
        self._program_prio = {}
        self._logger = logging.getLogger('HeatpumpController')
        self._day_schedule()


    def configure_program(self, program, settings):
        assert(isinstance(program, Program))
        self._program_settings[program] = settings
        self._logger.debug(f'{str(program):15} = {str(settings)}')


    def _set_program(self, priority, program):
        self._logger.debug(f'Priority {priority} set to {str(program)}')
        self._program_prio[priority] = program
        self._apply_program()
        return schedule.CancelJob   # so that the job is not executed again


    def _apply_program(self):
        # Pick the current program (starting with the highest priority)
        for prio, program in reversed(sorted(self._program_prio.items())):
            if program is not None:
                self._heatpump.apply_settings(self._circuit, self._program_settings[program])
                self._logger.info(f'Applied {program} (priority {prio})')
                return

        self._logger.error('No program available')


    def _day_schedule(self):
        self._logger.debug('Creating daily schedule')
        schedule.clear()
        schedule.every().day.at("00:00").do(lambda: self._day_schedule())
        self._base_schedule()
        # TODO fotowoltaika


    def _base_schedule(self):
        # G12 taryfa nocna (strefa 2):
        # * lato (1 kwietnia-30 wrzesnia):  15:00-17:00 i 22:00-6:00
        # * zima (1 pazdziernika-31 marca): 13:00-15:00 i 22:00-6:00

        if HeatpumpController._is_summer():     # lato; od 1 kwietnia do 30 wrzesnia
            self._schedule_program("06:00", 0, Program.LOW)
            self._schedule_program("15:00", 0, Program.HIGH)
            self._schedule_program("17:00", 0, Program.LOW)
            self._schedule_program("22:00", 0, Program.MEDIUM)
        else:                           # zima; od 1 pazdziernika do 31 marca
            self._schedule_program("06:00", 0, Program.SUSTAIN)
            self._schedule_program("13:00", 0, Program.HIGH)
            self._schedule_program("15:00", 0, Program.SUSTAIN)
            self._schedule_program("22:00", 0, Program.MEDIUM)


    def _schedule_program(self, hour, priority, program):
        self._logger.debug(f'  Schedule {program} at {hour} (prio {priority})')
        schedule.every().day.at(hour).do(lambda: self._set_program(priority, program))


    @staticmethod
    def _is_summer():
        today = datetime.date.today()
        summer_start = datetime.date(today.year, 4, 1)
        summer_end = datetime.date(today.year, 9, 30)
        return summer_start <= today <= summer_end


    def loop(self):
        schedule.run_pending()
