#!/usr/bin/env python3
from heatpump import *
from controller import *
import config as cfg
import time
import logging
import sys
import argparse

parser = argparse.ArgumentParser(prog='Heatpump Controller')
parser.add_argument('-v', '--verbose', action='store_true', help='Enables verbose logs')
parser.add_argument('--test', action='store_true', help='Runs in test mode (no communication with the heatpump')
parser.add_argument('--program', choices=['sustain', 'low', 'medium', 'high'], help='Applies selected temperature program on startup')
args = parser.parse_args()

logging.basicConfig(level=logging.DEBUG if args.verbose else logging.INFO,
        format='%(asctime)s|%(name)18s|%(levelname)7s: %(message)s',
        datefmt='%d-%m-%Y %H:%M:%S')

# wg. instrukcji: temperatura wody min=20*C, temperatura wody max=60*C
# zakres web: 20-60 *C dla kazdej temperatury powietrza
# ograniczenie temperatury wody zasilajacej wzgledem temperatury powietrza na zewnatrz:
# Tzew  | Max (instrukcja)
# -25*C | 45*C
# -20*C | 50*C
# -15*C | 53*C
# -10*C | 55*C
#  -5*C | 60*C
#   0*C | 60*C
# +15*C | 60*C

# Ustawienia programow grzania
sustain = TemperatureSettings({'Tm20': 50, 'Tm10': 45, 'T0': 38, 'Tp15': 31}, 22)
low     = TemperatureSettings({'Tm20': 50, 'Tm10': 46, 'T0': 42, 'Tp15': 33}, 23)
high    = TemperatureSettings({'Tm20': 50, 'Tm10': 48, 'T0': 45, 'Tp15': 36}, 25)

# Srednia krzywa grzewcza: wartosci srednie miedzy low i high
medium_curve = {}
for k in TemperatureSettings.CURVE_POINTS:
    medium_curve[k] = int((high.curve[k] + low.curve[k]) / 2)
medium_setpoint = int((high.setpoint + low.setpoint) / 2)
medium  = TemperatureSettings(medium_curve, medium_setpoint)

heatpump = HeatpumpLazar(cfg.hks_user, cfg.hks_password, verify_ssl=True, test=args.test)
controller = HeatpumpController(heatpump, 0, cfg.longitude, cfg.latitude)
logging.info('Configuring heating programs')
controller.configure_program(Program.SUSTAIN, sustain)
controller.configure_program(Program.LOW, low)
controller.configure_program(Program.MEDIUM, medium)
controller.configure_program(Program.HIGH, high)

# Schedule (bez fotowoltaiki)
if args.program:
    if args.program == 'high':
        logging.info('Applying high power settings')
        heatpump.apply_settings(0, high)
    elif args.program == 'medium':
        logging.info('Applying medium power settings')
        heatpump.apply_settings(0, medium)
    elif args.program == 'low':
        logging.info('Applying low power settings')
        heatpump.apply_settings(0, low)
    elif args.program == 'sustain':
        logging.info('Applying sustain power settings')
        heatpump.apply_settings(0, sustain)
    else:
        raise RuntimeError(f'Unknown program {args.program}')

logging.info('Starting the controller')

while True:
    controller.loop()
    time.sleep(15)
