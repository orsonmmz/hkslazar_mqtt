class MqttMockup:
    def _generic_func(self, name, *args, **kwargs):
        print('MqttMockup::{0} {1} {2}'.format(name, *args, **kwargs))

    def __init__(self, *args, **kwargs):
        self._generic_func('__init__', args, kwargs)

    def connect(self, *args, **kwargs):
        self._generic_func('connect', args, kwargs)

    def disconnect(self, *args, **kwargs):
        self._generic_func('disconnect', args, kwargs)

    def loop_start(self, *args, **kwargs):
        self._generic_func('loop_start', args, kwargs)

    def loop_stop(self, *args, **kwargs):
        self._generic_func('loop_stop', args, kwargs)

    def publish(self, *args, **kwargs):
        self._generic_func('publish', args, kwargs)
